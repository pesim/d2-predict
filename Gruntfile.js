/* eslint strict:off, global-require:off, no-unreachable:off*/
"use strict";
const _ = require('underscore');
const path= require('path');
const fs= require('fs');
const google = require('googleapis');
const trainedmodels= google.prediction('v1.6').trainedmodels;
const storage= google.storage('v1').objects;

module.exports= grunt => {
  const execute= (api, params) => new Promise((resolve, reject) =>
    google.auth.getApplicationDefault((err, authClient) =>
      err? reject(err): resolve(authClient.createScoped([
          'https://www.googleapis.com/auth/prediction',
          'https://www.googleapis.com/auth/devstorage.read_write']))
    ))
    .then(auth => new Promise((resolv, reject) =>
      api(_.extend({ auth }, params), (err, res) =>
      err? reject(err): resolv(res))))
    ;

  grunt.registerTask('list', 'list models', function(project) {
    const done= this.async();
    execute(trainedmodels.list, { project })
    .then(_.compose(done, grunt.log.ok, _.partial(_.pluck, _, 'id'), 
        _.property('items')))
    .catch(_.compose(done, grunt.log.error));
  });

  grunt.registerTask('insert', function(project, id, storageDataLocation) {
    const done= this.async();
    execute(trainedmodels.insert, 
      { project, resource: { id, storageDataLocation, modelType: 'CLASSIFICATION' } })
    .then(_.compose(done, grunt.log.ok, JSON.stringify))
    .catch(_.compose(done, grunt.log.error));
  });

  grunt.registerTask('get', 'get model status', function(project, id) {
    const done= this.async();
    execute(trainedmodels.get, { project, id })
    .then(_.compose(done, grunt.log.ok, JSON.stringify))
    .catch(_.compose(done, grunt.log.error));
  });

  grunt.registerTask('analyze', 'analyze models', function(project, id) {
    const done= this.async();
    execute(trainedmodels.analyze, { project, id })
    .then(_.compose(done, grunt.log.ok, JSON.stringify))
    .catch(_.compose(done, grunt.log.error));
  });

  grunt.registerTask('predict', function(project, id, ifname) {
    const done= this.async();
    const rows= fs.readFileSync(ifname, 'utf8').split('\n');
    Promise.all(rows.map(row => {
      if (row.length== 0) return;
      return execute(trainedmodels.predict, { project, id, resource: {
        input: { csvInstance: JSON.parse('['+ row+ ']') } } });
    }))
    .then(_.compose(done, _.partial(_.each, _, (e, i) =>
      e && console.log(e.outputMulti[0].score+ ','+ rows[i]))))
    .catch(_.compose(done, grunt.log.error));
  });

  grunt.registerTask('delete', 'delete model', function(project, id) {
    const done= this.async();
    execute(trainedmodels.delete, { project, id })
    .then(_.compose(done, grunt.log.ok, JSON.stringify))
    .catch(_.compose(done, grunt.log.error));
  });

  grunt.registerTask('ls', 'List storage', function(bucket) {
    const done= this.async();
    execute(storage.list, { bucket })
    .then(_.compose(done, grunt.log.ok, _.partial(_.pluck, _, 'name'), 
        _.property('items')))
    .catch(_.compose(done, grunt.log.error))
    ;
  });

  grunt.registerTask('upload', 'Upload file', function(bucket, name) {
    const done= this.async();
    const upload= fs.createReadStream(path.join(__dirname, name));
    execute(storage.insert, { bucket, name,
      media: { mimeType: 'text/plain', body: fs.readFileSync(name) } })
    .then(_.compose(done, grunt.log.ok, JSON.stringify))
    .catch(_.compose(done, grunt.log.error))
    ;
  });

  grunt.registerTask('rm', 'Delete file', function(bucket, name) {
    const done= this.async();
    execute(storage.delete, { bucket, object: name })
    .then(_.compose(done, grunt.log.ok, JSON.stringify))
    .catch(_.compose(done, grunt.log.error))
    ;
  });

  grunt.registerTask('dump-db', function(db, tbl, of) {
    const done= this.async();
    const output= fs.createWriteStream(of);
    require('knex')({
      client: 'sqlite3',
      connection: { filename: db },
      useNullAsDefault: true,
      acquireConnectionTimeout: 60* 25* 1000,
    })(tbl).select()
    .then(rows => {
      rows.forEach(r => {
        const result= [];
        if (r.last_ts== r.ts) return;
        if (r.next_ts== -1) {
          if (r.last_ts> Date.now()/ 1000- 4*24*3600) return;
          result.push('T');
        } else {
          result.push(r.next_ts> 60* 1000? 'T': 'F');
        }
        result.push(r.last_ts- r.ts);
        if (r.arena_cnt== 0) result.push(0, 0, 0); 
        else {
          result.push(r.arena_score/ r.arena_cnt);
          result.push(r.arena_result/ r.arena_cnt);
          result.push(r.arena_cnt);
        }
        if (r.basic_cnt== 0) result.push(0, 0, 0);
        else {
          result.push(r.basic_rating/ r.basic_cnt);
          result.push(r.basic_ticket/ r.basic_cnt);
          result.push(r.basic_cnt);
        }
        if (r.daily_cnt== 0) result.push(0, 0);
        else {
          result.push(r.daily_result/ r.daily_cnt);
          result.push(r.daily_cnt);
        }
        if (r.tower_cnt== 0) result.push(0, 0);
        else {
          result.push(r.tower_result/ r.tower_cnt);
          result.push(r.tower_cnt);
        }
        result.push(r.raid_cnt);
        result.push(r.devil_cnt);
        output.write(result.join(','));
        output.write('\n');
      });
      done();
    })
    .catch(_.compose(done, grunt.log.error))
    ;
  });
};
