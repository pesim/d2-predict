/* eslint strict:off, global-require:off, no-unreachable:off*/
'use strict';
const _ = require('underscore');
const async= require('async');
const fs= require('fs');

const now= Date.now();
const db_info= require('knex')({
  client: 'mysql',
  connection: process.env.MYSQL_URL
});

const db_local= require('knex')({
  client: 'sqlite3',
  connection: { filename: 'db_features' },
  useNullAsDefault: true,
  acquireConnectionTimeout: 60* 25* 1000,
});

class Feature {
  constructor(ts) {
    this.ts= ts;
    this.last_ts= ts;
    this.arena_score= 0;
    this.arena_result= 0;
    this.arena_cnt= 0;

    this.basic_rating= 0;
    this.basic_ticket= 0;
    this.basic_cnt= 0;

    this.daily_result= 0;
    this.daily_cnt= 0;

    this.tower_result= 0;
    this.tower_cnt= 0;

    this.raid_cnt= 0;

    this.devil_cnt= 0;
  }

  get total_cnt() { 
    return this.arena_cnt+ this.basic_cnt+ this.daily_cnt+ this.tower_cnt+
      this.raid_cnt+ this.devil_cnt;
  }

  arena(d) {
    this.arena_score+= d.gain_score;
    this.arena_result+= d.stage_result;
    this.arena_cnt+= 1;
    if (this.last_ts< d.log_time) this.last_ts= d.log_time;
  }

  basic(d) {
    this.basic_rating+= d.clear_rating;
    this.basic_ticket+= d.ticket_use;
    this.basic_cnt+= 1;
    if (this.last_ts< d.log_time) this.last_ts= d.log_time;
  }

  daily(d) {
    this.daily_result+= d.stage_result;
    this.daily_cnt+= 1;
    if (this.last_ts< d.log_time) this.last_ts= d.log_time;
  }

  tower(d) {
    this.tower_result+= d.stage_result;
    this.tower_cnt+= 1;
    if (this.last_ts< d.log_time) this.last_ts= d.log_time;
  }

  raid(d) {
    this.raid_cnt+= 1;
    if (this.last_ts< d.log_time) this.last_ts= d.log_time;
  }

  devil(d) {
    this.devil_cnt+= 1;
    if (this.last_ts< d.log_time) this.last_ts= d.log_time;
  }
}

class User {
  constructor(id) {
    this.id= id;
    this.features= [];
  }
  login(ts) {
    this.features.push(new Feature(ts));
  }
}

const selectRow= (tbl, column) => 
  db_info(tbl)
  .select([db_info.raw('unix_timestamp(log_time) as log_time'), 
    db_info.raw('(player_key& 0xffffffff| (player_key >> 24)) as player_key')]
      .concat(column))
  .whereIn('player_key', db_info('player_create').select('player_key'))
  .orderBy('id');

const users= {};
db_local.schema.createTableIfNotExists('features', tbl => {
  tbl.integer('player_key');
  tbl.integer('ts');
  tbl.integer('last_ts');
  tbl.integer('next_ts');

  tbl.integer('arena_score');
  tbl.integer('arena_result');
  tbl.integer('arena_cnt');

  tbl.integer('basic_rating');
  tbl.integer('basic_ticket');
  tbl.integer('basic_cnt');

  tbl.integer('daily_result');
  tbl.integer('daily_cnt');

  tbl.integer('tower_result');
  tbl.integer('tower_cnt');

  tbl.integer('raid_cnt');

  tbl.integer('devil_cnt');
})
.then(() => db_local('features').select())
.then(rows => {
  rows.forEach(r => {
    const result= [];
    if (r.last_ts== r.ts) return;
    if (r.next_ts== -1) {
      if (r.last_ts> Date.now()/ 1000- 4*24*3600) return;
      result.push(1);
    } else {
      result.push(r.next_ts> 60* 1000? 1: 0);
    }
    result.push(r.last_ts- r.ts);
    result.push(_.tail(_.values(r), 4));
    console.log(_.flatten(result).join(','));
  });
  process.exit(0);
})
.then(() => selectRow('player_select', []))
.then(_.partial(_.each, _, s => {
  if (!_.has(users, s.player_key))  {
    users[s.player_key]= new User(s.player_key);
  }
  users[s.player_key].login(s.log_time);
}))
.then(() => selectRow('stage_arena', ['gain_score', 'stage_result']))
.then(_.partial(_.each, _, s => {
  const i= _.sortedIndex(users[s.player_key].features, {ts: s.log_time}, 'ts');
  if (i== 0) console.log('Error', s.log_time);
  users[s.player_key].features[i- 1].arena(s);
}))
.then(() => selectRow('stage_basic', ['clear_rating', 'ticket_use']))
.then(_.partial(_.each, _, s => {
  const i= _.sortedIndex(users[s.player_key].features, {ts: s.log_time}, 'ts');
  if (i== 0) console.log('Error', s.log_time);
  users[s.player_key].features[i- 1].basic(s);
}))
.then(() => selectRow('stage_daily', 'stage_result'))
.then(_.partial(_.each, _, s => {
  const i= _.sortedIndex(users[s.player_key].features, {ts: s.log_time}, 'ts');
  if (i== 0) console.log('Error', s.log_time);
  users[s.player_key].features[i- 1].daily(s);
}))
.then(() => selectRow('stage_tower', 'stage_result'))
.then(_.partial(_.each, _, s => {
  const i= _.sortedIndex(users[s.player_key].features, {ts: s.log_time}, 'ts');
  if (i== 0) console.log('Error', s.log_time);
  users[s.player_key].features[i- 1].tower(s);
}))
.then(() => selectRow('stage_raid', []))
.then(_.partial(_.each, _, s => {
  const i= _.sortedIndex(users[s.player_key].features, {ts: s.log_time}, 'ts');
  if (i== 0) console.log('Error', s.log_time);
  users[s.player_key].features[i- 1].raid(s);
}))
.then(() => selectRow('stage_devil_war', []))
.then(_.partial(_.each, _, s => {
  const i= _.sortedIndex(users[s.player_key].features, {ts: s.log_time}, 'ts');
  if (i== 0) console.log('Error', s.log_time);
  users[s.player_key].features[i- 1].devil(s);
}))
.then(() => Promise.all(
  _.chain(users).values().map(u => {
    const f= _.map(u.features, (v, i, l) => 
      _.extend(v, (i== l.length- 1)? { next_ts: -1, player_key: u.id }: 
        { next_ts: l[i+ 1].ts- v.last_ts, player_key: u.id })
    );
    return db_local.batchInsert('features', f, 50);
  }).value()
))
.then(() => console.log(_.keys(users).length))
.catch(_.partial(console.log, 'Error'))
;
